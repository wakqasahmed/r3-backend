// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies
var mongoose = require('mongoose'),
	Element = mongoose.model('Element'),
	uuid = require('node-uuid'),
	multiparty = require('multiparty'),
	fs = require('fs'),
	vision = require('node-cloud-vision-api');

// Create a new error handling controller method
var getErrorMessage = function(err) {
	if (err.errors) {
		for (var errName in err.errors) {
			if (err.errors[errName].message) return err.errors[errName].message;
		}
	} else {
		return 'Unknown server error';
	}
};

exports.postImage = function(req, res) {
	var form = new multiparty.Form();
	form.parse(req, function(err, fields, files) {
	    var file = files.file[0];
	    var contentType = file.headers['content-type'];
	    var tmpPath = file.path;
	    var extIndex = tmpPath.lastIndexOf('.');
	    var extension = (extIndex < 0) ? '' : tmpPath.substr(extIndex);
	    // uuid is for generating unique filenames.
	    var fileName = uuid.v4() + extension;
	    var destPath = appRoot + '/../public/content/element_images/' + fileName;
			var pathToSave = fileName;

	    // Server side file type checker.
	    if (contentType !== 'image/png' && contentType !== 'image/jpeg') {
	        fs.unlink(tmpPath);
	        return res.status(400).send('Unsupported file type.');
	    }

	    var is = fs.createReadStream(tmpPath);
	    var os = fs.createWriteStream(destPath);

	    if(is.pipe(os)) {
	        fs.unlink(tmpPath, function (err) { //To unlink the file from temp path after copy
	            if (err) {
	                console.log(err);
	            }
	        });
	        return res.json(pathToSave);
	    }else
	        return res.json('File not uploaded');
	});
};

exports.elementDataByIdentifier = function(req, res){

	console.log(req.params.identifier);
	// Use the model 'find' method to get the element by identifier
	Element.where('identifier').equals(req.params.identifier).exec(function(err, element) {
		if (err) {
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the element
			res.json(element);
		}
	});	
}

exports.recognizeElementByUrl = function(req, res){
	console.log("in recognize element");
	/*
	console.log(req.files);
	var imagePath = clientPostImage(req, res);
	console.log(imagePath);
	if(imagePath.status == 200) {
		console.log("ballay ballay.. now identify object: " + imagePath.path);
	} else {
		console.log("something went wrong.. " + imagePath.message);
	}
	*/
	var imagePath = "http://images.indianexpress.com/2015/12/iphone-5s-big.jpg";

	if(req.query.image){
		console.log('request has an image it seems');
		imagePath = req.query.image;
	}

	// init with auth
	vision.init({auth: 'AIzaSyBPxt4mB9EIyaE77QawioWHuW0OKbCR8uA'})
	//vision.init();
	console.log("Vision initiated");

	// construct parameters
	const gcloudReq = new vision.Request({
	  image: new vision.Image({
	    url: imagePath
	  }),
	  features: [
	    new vision.Feature('LABEL_DETECTION', 10)
	  ]
	});

	console.log('calling annotate');
	// send single request
	vision.annotate(gcloudReq).then((gcloudRes) => {
		console.log('in response');
		// handling response
		var response = JSON.stringify(gcloudRes.responses[0].labelAnnotations[0].description);
		return res.send(response);  
		
		//console.log(JSON.stringify(gcloudRes.responses))
	}, (e) => {
	  	console.log('Error: ', e);			
		return res.send({"status": "error", "message": e});			
	});	
}

exports.recognizeElement = function(req, res){

	var imagePath = clientPostImage(req, res);
	console.log(imagePath);
	if(imagePath.status == 200) {
		console.log("ballay ballay.. now identify object: " + imagePath.path);
	} else {
		console.log("something went wrong.. " + imagePath.message);
	}

	// init with auth
	vision.init({auth: 'AIzaSyBPxt4mB9EIyaE77QawioWHuW0OKbCR8uA'})
	//vision.init();
	console.log("Vision initiated");

	// construct parameters
	const gcloudReq = new vision.Request({
	  image: new vision.Image({
	    url: imagePath.path
	  }),
	  features: [
	    new vision.Feature('LABEL_DETECTION', 10)
	  ]
	});

	console.log('calling annotate');
	// send single request
	vision.annotate(gcloudReq).then((gcloudRes) => {
		console.log('in response');
		// handling response
		var response = JSON.stringify(gcloudRes.responses[0].labelAnnotations[0].description);
		return res.send(response);  
		
		//console.log(JSON.stringify(gcloudRes.responses))
	}, (e) => {
	  	console.log('Error: ', e);			
		return res.send({"status": "error", "message": e});			
	});	
}

var clientPostImage = function(req, res) {
	var form = new multiparty.Form();
	form.parse(req, function(err, fields, files) {
	    var file = files.file[0];
	    var contentType = file.headers['content-type'];
	    var tmpPath = file.path;
	    var extIndex = tmpPath.lastIndexOf('.');
	    var extension = (extIndex < 0) ? '' : tmpPath.substr(extIndex);
	    // uuid is for generating unique filenames.
	    var fileName = uuid.v4() + extension;
	    var destPath = appRoot + '/../public/content/user_uploaded_element_images/' + fileName;
			var pathToSave = fileName;

	    // Server side file type checker.
	    if (contentType !== 'image/png' && contentType !== 'image/jpeg') {
	        fs.unlink(tmpPath);
	        return res.status(400).send({ message: 'Unsupported file type.' });
	    }

	    var is = fs.createReadStream(tmpPath);
	    var os = fs.createWriteStream(destPath);

	    if(is.pipe(os)) {
	        fs.unlink(tmpPath, function (err) { //To unlink the file from temp path after copy
	            if (err) {
	                console.log(err);
	            }
	        });
	        return res.status(200).send({ path: pathToSave });
	    } else {
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		}	    

	});
};

// Create a new controller method that creates new elements
exports.create = function(req, res) {
	// Create a new element object
	var element = new Element(req.body);

	// Set the element's 'createdBy' property
	element.createdBy = req.user;

	// Set the element's 'createdOn' property
	element.createdOn = Date.now();

	console.log(element);

	// Try saving the element
	element.save(function(err) {
		if (err) {
			console.log('error found');
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the element
			res.json(element);
		}
	});
};

// Create a new controller method that retrieves a list of elements with status 'active'
exports.listActiveElements = function(req, res) {
	// Use the model 'find' method to get a list of elements
	Element.where('status').equals('active').exec(function(err, elements) {
		if (err) {
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the element
			res.json(elements);
		}
	});
};

// Create a new controller method that retrieves a list of elements
exports.list = function(req, res) {

	if(req.query.skip && req.query.take) {

		var totalRecords;

		var page = parseInt(req.query.page),
			size = parseInt(req.query.pageSize),
			skip = parseInt(req.query.skip),
			take = parseInt(req.query.take);
//		skip = page > 0 ? ((page - 1) * size) : 0;

		Element.count({}, function(err, count){
			if (err) {
				// If an error occurs send the error message
				return res.status(400).send({
					message: getErrorMessage(err)
				});
			} else {
				totalRecords = count;
				// Use the model 'find' method to get a list of elements
				Element.find().limit(size).skip(skip).exec(function (err, elements) {
					if (err) {
						// If an error occurs send the error message
						return res.status(400).send({
							message: getErrorMessage(err)
						});
					} else {
						// Send a JSON representation of the element
						res.json({elements: elements, totalRecords: totalRecords});
					}
				});
			}
		});

	}
	else {
		// Use the model 'find' method to get a list of elements
		Element.find().exec(function (err, elements) {
			if (err) {
				// If an error occurs send the error message
				return res.status(400).send({
					message: getErrorMessage(err)
				});
			} else {
				// Send a JSON representation of the element
				res.json(elements);
			}
		});
	}

};

// Create a new controller method that returns an existing element
exports.read = function(req, res) {
	res.json(req.element);
};

// Create a new controller method that updates an existing element
exports.update = function(req, res) {
	// Get the element from the 'request' object
	var element = req.element;

	// Update the element fields
	element.title = req.body.title;
	element.content = req.body.content;

	// Try saving the updated element
	element.save(function(err) {
		if (err) {
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the element
			res.json(element);
		}
	});
};

// Create a new controller method that delete an existing element
exports.delete = function(req, res) {
	// Get the element from the 'request' object
	var element = req.element;

	// Use the model 'remove' method to delete the element
	element.remove(function(err) {
		if (err) {
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the element
			res.json(element);
		}
	});
};

// Create a new controller middleware that retrieves a single existing element
exports.elementByID = function(req, res, next, id) {
	// Use the model 'findById' method to find a single element
	Element.findById(id).populate('creator', 'firstName lastName fullName').exec(function(err, element) {
		if (err) return next(err);
		if (!element) return next(new Error('Failed to load element ' + id));

		// If an element is found use the 'request' object to pass it to the next middleware
		req.element = element;

		// Call the next middleware
		next();
	});
};

// Create a new controller middleware that is used to authorize an element operation
exports.hasAuthorization = function(req, res, next) {
	// If the current user is not the creator of the element send the appropriate error message
	if (req.user.role !== 'admin') {
		return res.status(403).send({
			message: 'User is not authorized'
		});
	}

	// Call the next middleware
	next();
};
