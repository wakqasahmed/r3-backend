// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  moment = require('moment-timezone');

var statuses = 'active inactive'.split(' ');

var imagesSchema = new Schema({
  src: String,
  caption: String
});

// Define a new 'brandSchema'
var elementSchema = new Schema({
  name: {type: String, trim: true},
  identifier: {type: String, unique: true, trim: true},  
  status: { type: String, enum: statuses, "default": 'active' },
  images: [imagesSchema],
  wasteType: String,
  about: String,
  reduce: String,
  reuse: String,
  recycle: String,
  createdOn: {type: Date},
  modifiedOn: {type: Date, default: moment.tz(Date.now(), 'Asia/Dubai')},
  createdBy: { type: Schema.ObjectId, ref: 'User' }
}, { collection : 'elements' });

// Create the 'Element' model out of the 'brandSchema'
mongoose.model('Element', elementSchema);
