// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies
var users = require('../../app/controllers/users.server.controller'),
	elements = require('../../app/controllers/elements.server.controller');

// Define the routes module' method
module.exports = function(app) {
	// Set up the 'elements' base routes
	app.route('/api/elements')
		.get(elements.list)
//		.post(elements.create);
		.post(users.requiresLogin, elements.create);

app.route('/api/elements/active')
	.get(elements.listActiveElements);

app.route('/api/elements/uploadUserElement')
		.post(elements.recognizeElement);

app.route('/api/elements/recognizeElementByUrl')
		.get(elements.recognizeElementByUrl);

app.route('/api/elements/data/:identifier')
		.get(elements.elementDataByIdentifier);

app.route('/api/elements/upload')
    .post(users.requiresLogin, elements.postImage);
//		.post(elements.postImage);

	// Set up the 'elements' parameterized routes
	app.route('/api/elements/:elementId')
		.get(elements.read)
		.put(users.requiresLogin, elements.hasAuthorization, elements.update)
		.delete(users.requiresLogin, elements.hasAuthorization, elements.delete);

	// Set up the 'elementId' parameter middleware
	//app.param('elementId', elements.elementByID);
};
